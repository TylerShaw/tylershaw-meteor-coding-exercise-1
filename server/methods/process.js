/*****************************************************************************/
/* Process Methods */
/*****************************************************************************/

Meteor.methods({
'/app/process': function (player) {
      
      var message = 'Processed: ' + JSON.stringify(player);
      console.log(message);
      
      Player.insert(player);
      
      var players = Player.find({}).fetch();
      
      return players;
}
});